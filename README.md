# Processo para projetos envolvendo WebServices de terceiros #

### O que tem neste documento###

1. [Introdução](https://bitbucket.org/ippRJ/processo-desenvolvimento-servicos/wiki/Introducao)
2. [Documentação](https://bitbucket.org/ippRJ/processo-desenvolvimento-servicos/wiki/Documentacao)
3. [Como usar](https://bitbucket.org/ippRJ/processo-desenvolvimento-servicos/wiki/ComoUsar)
4. [Cenários de erro](https://bitbucket.org/ippRJ/processo-desenvolvimento-servicos/wiki/CenariosDeErro)
5. [Massa de testes](https://bitbucket.org/ippRJ/processo-desenvolvimento-servicos/wiki/MassaDeTestes)